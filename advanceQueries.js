// Advance Query Operators
//      We want more flexible querying of data within MongoDB.

// [SECTION] Comparison Query Operators
/* 

    $gt/$gte operator

    - Allows us to find documents that have field number values greater than or equal to specified value.

    Syntax:

        db.collectionName.find({field: {$gt/$gte: value}});


*/


// This will find age >= 65
db.users.find(
    {
        age: {    $gte: 65
        }
    }
    );


/* 

        $lt/lte operator

        - Allows us to find documents that have the field number values less than or equal to a specified value.

        db.collectionName.find({field: {$lt/$lte: value}});        

*/

// This will find age < 65
db.users.find(
    {
        age: {    $lt: 65
        }
    }
    );

/* 

    $ne operators

    - Allows us to find documents field number values not equal to a specified value.

    Syntax:

        db.collectionName.find({field: {$ne: value}}); 

*/

db.users.find({age: {$ne: 82}}); 



/* 

    $in operator

    - Allows us to find documents with spefic match criteria with one field using different values in ARRAY.

    Syntax:

        db.collectionName.find({field: {$in: [valueA, valueB]}}); 

*/

db.users.find({lastName: {$in: ["Pineda", "Doe"]}});


db.users.find({
    courses: {
        $in: ["HTML", "React"]
    }
});


//  [SECTION] Logical Query Operators

/* 

      
        $or
        
        -   Joins query clauses with a logical OR returns all documents that match the conditions of either clause.


        db.collectionName.find({    $or:[{fieldA: ValueA}, {fieldA: ValueA},]});

        
        
*/


        db.users.find({$or: [{age: 65},{firstName: "Joel"}]});

        db.users.find({ $or:[{firstName: "Neil"},{age: {$gt:25}}]});




/* 
    $and Operator

    


*/



db.users.find({
    $and: [
        {age: {$ne: 82}},
        {age: {$ne: 76}}
    ]
});


db.users.find ({
    $and: [
        {age: {$ne:82}},
        {department: {$ne: "Operations"}}
    ]
});




//  [SECTION] Field Projection

//  To help with the readability of the values returned, we can include and exclude fields from the response.

//  Inclusion

/* 
    - Allows us to inclide/add specific fields only when retrieving a document.

        Syntax:

            db.collectionName:find({criteria}, {field: 1});

*/


db.users.find(
    {
    firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }    
);

// Exclusion

/* 
    - Allows us to exclude/remove specific fields only when retrieving documents.
    - The value provided is 0 to denote that fields is being excluded.


    Syntax:

        db.collectionName.find({criteria}, {field: 0});

*/

db.users.find({
    firstName: "Jane"
},
{
    contact: 0,
    department: 0
}
);

// Supressing the ID field
/* 
    - When using field projection, field inclusion and exclusion may not be used at the same time.
    - Excluding the "_id" field is the only exception to this rule.

        Syntax:

            db.collectionName.find({criteria}, {_id: 0});

*/

db.users.find(
    {
    firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1,
        _id: 0
    }    
);






//  [SECTION] Evaluation Query Operators

/*
        $regex operator

        - Allows us to find documents that match a specific string pattern using regular expressions.

        Syntax:

            db.collectionName.find({field: {$regex: "pattern", $options: $optionValue});
*/

//  case sensitive query

    db.users.find(
        {
            firstName: {
                $regex: 'N',
                $options: "$i"
            }
        }
    );



db.users.find(
    {
        firstName: "jane",
        $options: "$i"   
    }
);

